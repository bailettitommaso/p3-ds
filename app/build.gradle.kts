plugins {
    // Apply the application plugin to add support for building a CLI application in Java.
    application
}

repositories {
    // Use JCenter for resolving dependencies.
    jcenter()
}

dependencies {
    // Use JUnit test framework.
    testImplementation("junit:junit:4.13")

    // This dependency is used by the application.
    implementation("com.google.guava:guava:29.0-jre")
    implementation("io.vertx:vertx-core:4.0.3")
    implementation("io.vertx:vertx-web:4.0.3")
    implementation("io.vertx:vertx-web-validation:4.0.3")
    implementation("io.github.java-native:jssc:2.9.2")
}

application {
    // Define the main class for the application.
    mainClass.set("it.unibo.P3DS.App")
}

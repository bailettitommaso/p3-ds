package it.unibo.P3DS;

public class LastData {
    private AlarmEnum state;
    private int opening;
    private float level;
    private boolean manual;

    public LastData(AlarmEnum state, int opening, float level, boolean manual) {
        this.state = state;
        this.opening = opening;
        this.level = level;
        this.manual = manual;
    }

    public AlarmEnum getState() {
        return state;
    }

    public void setState(AlarmEnum state) {
        this.state = state;
    }

    public int getOpening() {
        return opening;
    }

    public void setOpening(int opening) {
        this.opening = opening;
    }

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    @Override
    public String toString() {
        return "LastData [state=" + state + ", opening=" + opening + ", level=" + level + ", manual=" + manual + "]";
    }


}

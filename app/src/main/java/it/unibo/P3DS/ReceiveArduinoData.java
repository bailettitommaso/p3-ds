package it.unibo.P3DS;

import java.util.concurrent.Semaphore;

public class ReceiveArduinoData implements Runnable {

    private final CommChannel channel;
    private final LastData lastData;
    private final Semaphore sem;

    public ReceiveArduinoData(CommChannel channel, LastData lastData, Semaphore sem) {
        this.channel = channel;
        this.lastData = lastData;
        this.sem = sem;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String s = this.channel.receiveMsg();
                App.LOG(s);
                if (s.startsWith("op:")) {
                    this.sem.acquire();
                    try {
                        this.lastData.setOpening(Integer.parseInt(s.substring(3)));
                    } catch (Exception e) {
                        App.LOG(e.toString());
                    }
                    this.sem.release();
                } else if (s.startsWith("md:")) {
                    App.LOG("Change mode");
                    this.sem.acquire();
                    try {
                        App.LOG(s.substring(3).equals("man"));
                        this.lastData.setManual(s.substring(3).equals("man"));
                        App.LOG(this.lastData);
                    } catch (Exception e) {
                        App.LOG(e.toString());
                    }
                    this.sem.release();
                }

                // CAMBIAREEEE
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}

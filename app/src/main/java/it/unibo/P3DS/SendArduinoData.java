package it.unibo.P3DS;

import java.util.concurrent.Semaphore;

public class SendArduinoData implements Runnable {

    private final CommChannel channel;
    private final LastData lastData;
    private final Semaphore sem;
	private int periodSend;

    public SendArduinoData(CommChannel channel, LastData lastData, Semaphore sem, int periodSend) {
        this.lastData = lastData;
        this.channel = channel;
        this.sem = sem;
        this.periodSend = periodSend;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(this.periodSend);
                this.sem.acquire();
                this.channel.sendMsg("lv:" + (WebServer.MAX_LENGHT - this.lastData.getLevel()));
                App.LOG("Sent to Arduino");
                this.sem.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

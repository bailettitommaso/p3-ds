package it.unibo.P3DS;

public enum AlarmEnum {
    NORMAL(0, 400),
    PRE_ALARM(1, 460),
    ALARM(2),
    MANUAL(3);

    private final int level;
    private final int code;

    AlarmEnum(int code) {
        this(code, 0);
    }

    AlarmEnum(int code, int level) {
        this.code = code;
        this.level = level;
    }

    public static AlarmEnum getAlarmLevel(float level) {
        if (level >= AlarmEnum.PRE_ALARM.getLevel()) {
            return AlarmEnum.ALARM;
        } else if (level >= AlarmEnum.NORMAL.getLevel()) {
            return AlarmEnum.PRE_ALARM;
        } else {
            return AlarmEnum.NORMAL;
        }
    }

    public int getLevel() {
        return level;
    }

    public int getCode() {
        return code;
    }
}

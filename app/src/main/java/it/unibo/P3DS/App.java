/*
 *************************************
 * PROGETTO SMART DAM REALIZZATO DA
 * ANGELO TINTI e TOMMASO BAILETTI
 *************************************
 */
package it.unibo.P3DS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;


public class App {
    final private static String PORT_NAME = "COM5";
    final private static int MILLISECONDS_PERIOD_SEND_DATA = 3000;
    final private static int PORT_BAUDRATE = 9600;
    final private static AlarmEnum INITIAL_STATE = AlarmEnum.NORMAL;
    final private static float INITIAL_LEVEL = 0;
    final private static int INITIAL_OPENING = 0;
    final private static boolean INITIAL_MODE = false;

    public static void LOG(Object s) {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()) + " - " + s);
    }

    public static void main(String[] args) throws Exception {
        CommChannel channel = new SerialCommChannel(PORT_NAME, PORT_BAUDRATE);
        LOG("Waiting Arduino for rebooting...");
        Thread.sleep(4000);
        LOG("Ready.");
        LastData lastData = new LastData(INITIAL_STATE, INITIAL_OPENING, INITIAL_LEVEL, INITIAL_MODE);
        Semaphore sem = new Semaphore(1);

        new Thread(new WebServer(lastData, sem)).start();
        new Thread(new SendArduinoData(channel, lastData, sem, MILLISECONDS_PERIOD_SEND_DATA)).start();
        new Thread(new ReceiveArduinoData(channel, lastData, sem)).start();
    }


}

package it.unibo.P3DS;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.validation.BadRequestException;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.ext.web.validation.builder.Bodies;
import io.vertx.json.schema.SchemaParser;
import io.vertx.json.schema.SchemaRouter;
import io.vertx.json.schema.SchemaRouterOptions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import static io.vertx.json.schema.common.dsl.Schemas.numberSchema;
import static io.vertx.json.schema.common.dsl.Schemas.objectSchema;

public class WebServer implements Runnable {

    public static final int MAX_LENGHT = 500;
    private static final int MAX_RECORDS = 20;
    
    private final Map<Long, Float> logs = Collections.synchronizedMap(new HashMap<>());
 
    private final LastData lastData;
    private final Semaphore sem;

    public WebServer(LastData lastData, Semaphore sem) {
        this.lastData = lastData;
        this.sem = sem;
    }

    @Override
    public void run() {
        // Define Vertx instance
        Vertx vertx = Vertx.vertx();
        // Define schemas for validation purposes
        SchemaRouter schemaRouter = SchemaRouter.create(vertx, new SchemaRouterOptions());
        SchemaParser schemaParser = SchemaParser.createDraft201909SchemaParser(schemaRouter);
        // Instantiate Server
        HttpServer server = vertx.createHttpServer();
        // Instantiate router and routes
        Router router = Router.router(vertx);
        // Tell Vertx to handle and manage the body in the requests. Just, WHY?
        router.route().handler(BodyHandler.create());
        router.get("/").handler(StaticHandler.create());
        router.get("/status")
                .handler(ctx -> {
                            //App.LOG("Status request");
                            try {
                                this.sem.acquire();
                                AlarmEnum state = AlarmEnum.getAlarmLevel(this.lastData.getLevel());
                                this.lastData.setState(state == AlarmEnum.ALARM ? (this.lastData.isManual() ? AlarmEnum.MANUAL : state) : state);
                                ctx.json(new JsonObject()
                                        .put("status", this.lastData.getState().getCode())
                                        // get request add last value registered in answer if exist, zero otherwise
                                        .put("lastVal", this.lastData.getLevel())
                                        .put("lastOp", this.lastData.getOpening()));

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                this.sem.release();
                            }
                        }
                        //this.lastData.setState(this.getAlarmLevel());
                );
        router.get("/level")
                .handler(ctx -> ctx.json(new JsonObject().put("data",
                        logs.entrySet().stream()
                                .sorted(Map.Entry.comparingByKey())
                                .skip(Math.max(0, logs.size() - MAX_RECORDS))
                                .map(entry -> new JsonArray()
                                        .add(0, entry.getKey())
                                        .add(1, entry.getValue()))
                                .collect(JsonArray::new, JsonArray::add, JsonArray::addAll))
                ));
        router.post("/level")
                .handler(
                        ValidationHandler.builder(schemaParser)
                                .body(Bodies.json(objectSchema().requiredProperty("value", numberSchema())))
                                .build()
                )
                .handler(ctx -> {
                    RequestParameters parameters = ctx.get(ValidationHandler.REQUEST_CONTEXT_KEY);
                    float lvl = MAX_LENGHT - Float.parseFloat(parameters.body().getJsonObject().getValue("value").toString());
                    try {
                        this.sem.acquire();
                        this.lastData.setLevel(lvl);
                        AlarmEnum state = AlarmEnum.getAlarmLevel(lvl);
                        this.lastData.setState(state == AlarmEnum.ALARM ? (this.lastData.isManual() ? AlarmEnum.MANUAL : state) : state);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        this.sem.release();
                    }
                    App.LOG(this.lastData.toString());
                    logs.put(System.currentTimeMillis(), lvl);
                    ctx.json(new JsonObject().put("Message", "Entry received."));
                    App.LOG("Level received");
                });
        router.errorHandler(400, routingContext -> {
            if (routingContext.failure() instanceof BadRequestException) {
                System.out.println(routingContext.failure().getMessage());
            }
        });
        // set the router component in the server, then start itS
        server.requestHandler(router).listen(8080);
    }

}
